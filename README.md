# rg-scoring

Rhythmic gymnastics scoring system

## Установка:
1. Установить пакеты: в папках frontend и backend запустить команду yarn
2. `docker-compose -f "docker-compose.prod.yml" up -d --build`
3. В папке `backend` запустить `yarn setup`

## Backup/restore:
+ В pgadmin при restore ставить галочку clean before restore
+ Volume докера на винде хранятся по пути (вставить в строку проводника)
`\\wsl$\docker-desktop-data\version-pack-data\community\docker\volumes\`

frontend
>http://localhost

admin
>http://localhost/admin

prisma studio
>http://localhost:5555

server
>http://localhost/api

jsreport
>http://localhost/jsreport

traefik dashboard
>http://localhost:8080
