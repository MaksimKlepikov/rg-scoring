-- cat ./backend/prisma/db/schema.sql | docker exec -i rg-scoring_postgres_1 psql -U prisma -d prod

--- Some fields are written in double-quotes to ensure PostgreSQL uses proper casing. 
--- If no double-quotes were used, PostgreSQL would just read everything as lowercase characters.

CREATE TYPE "EnumCategoryType" AS ENUM (
    'INDIVIDUAL',
    'GROUP'
);
CREATE TYPE "EnumEventState" AS ENUM (
    'OPENED',
    'FINISHED'
);
CREATE TYPE "EnumJudgePanel" AS ENUM (
    'PANEL_1',
    'PANEL_2'
);
CREATE TYPE "EnumJudgeType" AS ENUM (
    'd1',
    'd2',
    'd3',
    'd4',
    'e1_2',
    'e3',
    'e4',
    'e5',
    'e6'
);

CREATE TABLE "Country" (
  id SERIAL PRIMARY KEY NOT NULL,
  name text NOT NULL UNIQUE,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL
);
CREATE TABLE "Region" (
  id SERIAL PRIMARY KEY NOT NULL,
  name text NOT NULL UNIQUE,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL
);
CREATE TABLE "City" (
  id SERIAL PRIMARY KEY NOT NULL,
  name text NOT NULL UNIQUE,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL
);

CREATE TABLE "Day" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  date TIMESTAMP NOT NULL,
  "eventId" integer
  -- FOREIGN KEY ("eventId") REFERENCES "Event"(id) ON DELETE SET NULL
);

CREATE TABLE "Event" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  name text NOT NULL,
  state "EnumEventState" DEFAULT 'OPENED'::"EnumEventState" NOT NULL,
  "countryId" INTEGER,
  "regionId" INTEGER,
  "cityId" INTEGER,
  "currentDayId" INTEGER,
  FOREIGN KEY ("countryId") REFERENCES "Country"(id) ON DELETE SET NULL,
  FOREIGN KEY ("regionId") REFERENCES "Region"(id) ON DELETE SET NULL,
  FOREIGN KEY ("cityId") REFERENCES "City"(id) ON DELETE SET NULL,
  FOREIGN KEY ("currentDayId") REFERENCES "Day"(id) ON DELETE SET NULL
);

ALTER TABLE ONLY public."Day"
    ADD FOREIGN KEY ("eventId") REFERENCES public."Event"(id) ON DELETE SET NULL;

CREATE TABLE "Category" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  name text NOT NULL,
  type "EnumCategoryType" NOT NULL
);

CREATE TABLE "Sportsman" (
  id SERIAL PRIMARY KEY NOT NULL,
  "numberInCategory" integer NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  "firstName" text NOT NULL,
  "lastName" text NOT NULL,
  "middleName" text NOT NULL,
  trainers text NOT NULL,
  "birthDate" text NOT NULL,
  rank text NOT NULL,
  club text NOT NULL,
  "countryId" INTEGER,
  "regionId" INTEGER,
  "cityId" INTEGER,
  FOREIGN KEY ("countryId") REFERENCES "Country"(id) ON DELETE SET NULL,
  FOREIGN KEY ("regionId") REFERENCES "Region"(id) ON DELETE SET NULL,
  FOREIGN KEY ("cityId") REFERENCES "City"(id) ON DELETE SET NULL
);

CREATE TABLE "SportsmanGroup" (
  id SERIAL PRIMARY KEY NOT NULL,
  "numberInCategory" integer NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  name text NOT NULL,
  trainers text NOT NULL,
  rank text NOT NULL,
  club text NOT NULL,
  "countryId" INTEGER,
  "regionId" INTEGER,
  "cityId" INTEGER,
  FOREIGN KEY ("countryId") REFERENCES "Country"(id) ON DELETE SET NULL,
  FOREIGN KEY ("regionId") REFERENCES "Region"(id) ON DELETE SET NULL,
  FOREIGN KEY ("cityId") REFERENCES "City"(id) ON DELETE SET NULL
);

CREATE TABLE "Kind" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  name text,
  "isApparatus" boolean NOT NULL
);

CREATE TABLE "Score" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  d1 numeric(7,5),
  d2 numeric(7,5),
  d3 numeric(7,5),
  d4 numeric(7,5),
  e1_2 numeric(7,5),
  e3 numeric(7,5),
  e4 numeric(7,5),
  e5 numeric(7,5),
  e6 numeric(7,5),
  "eAverage" numeric(7,5),
  "finalD" numeric(7,5),
  "finalE" numeric(7,5),
  "finalPenalty" numeric(7,5) DEFAULT 0 NOT NULL,
  "finalScore" numeric(7,5)
);

CREATE TABLE "Rotation" (
    id SERIAL PRIMARY KEY NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP NOT NULL,
    "mainNumber" integer NOT NULL,
    name text NOT NULL,
    "dayId" integer NOT NULL,
    FOREIGN KEY ("dayId") REFERENCES "Day"(id) ON DELETE CASCADE
);

CREATE TABLE "Performance" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  "mainNumber" integer NOT NULL,
  "numberInRotation" integer NOT NULL,
  "judgePanel" "EnumJudgePanel" DEFAULT 'PANEL_1'::"EnumJudgePanel" NOT NULL,
  "sportsmanId" integer,
  "sportsmanGroupId" integer,
  "kindId" integer NOT NULL,
  "scoreId" integer NOT NULL,
  "dayId" integer NOT NULL,
  "rotationId" integer NOT NULL,
  FOREIGN KEY ("sportsmanId") REFERENCES "Sportsman"(id) ON DELETE SET NULL,
  FOREIGN KEY ("sportsmanGroupId") REFERENCES "SportsmanGroup"(id) ON DELETE SET NULL,
  FOREIGN KEY ("kindId") REFERENCES "Kind"(id) ON DELETE CASCADE,
  FOREIGN KEY ("scoreId") REFERENCES "Score"(id) ON DELETE CASCADE,
  FOREIGN KEY ("dayId") REFERENCES "Day"(id) ON DELETE CASCADE,
  FOREIGN KEY ("rotationId") REFERENCES "Rotation"(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX "Performance_scoreId" ON "Performance"("scoreId" int4_ops);


CREATE TABLE "JudgeSeat" (
  id SERIAL PRIMARY KEY NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updatedAt" TIMESTAMP NOT NULL,
  type "EnumJudgeType" NOT NULL,
  panel "EnumJudgePanel" NOT NULL,
  "currentPerformanceId" integer,
  "eventId" integer NOT NULL,
  FOREIGN KEY ("currentPerformanceId") REFERENCES "Performance"(id) ON DELETE SET NULL,
  FOREIGN KEY ("eventId") REFERENCES "Event"(id) ON DELETE CASCADE
);

CREATE TABLE "_CategoryToEvent" (
    "A" integer NOT NULL REFERENCES "Category"(id) ON DELETE CASCADE,
    "B" integer NOT NULL REFERENCES "Event"(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX "_CategoryToEvent_AB_unique" ON "_CategoryToEvent"("A" int4_ops,"B" int4_ops);
CREATE INDEX "_CategoryToEvent_B_index" ON "_CategoryToEvent"("B" int4_ops);


CREATE TABLE "_CategoryToSportsman" (
    "A" integer NOT NULL REFERENCES "Category"(id) ON DELETE CASCADE,
    "B" integer NOT NULL REFERENCES "Sportsman"(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX "_CategoryToSportsman_AB_unique" ON "_CategoryToSportsman"("A" int4_ops,"B" int4_ops);
CREATE INDEX "_CategoryToSportsman_B_index" ON "_CategoryToSportsman"("B" int4_ops);


CREATE TABLE "_CategoryToSportsmanGroup" (
    "A" integer NOT NULL REFERENCES "Category"(id) ON DELETE CASCADE,
    "B" integer NOT NULL REFERENCES "SportsmanGroup"(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX "_CategoryToSportsmanGroup_AB_unique" ON "_CategoryToSportsmanGroup"("A" int4_ops,"B" int4_ops);
CREATE INDEX "_CategoryToSportsmanGroup_B_index" ON "_CategoryToSportsmanGroup"("B" int4_ops);


CREATE TABLE "_SportsmanToSportsmanGroup" (
    "A" integer NOT NULL REFERENCES "Sportsman"(id) ON DELETE CASCADE,
    "B" integer NOT NULL REFERENCES "SportsmanGroup"(id) ON DELETE CASCADE
);
CREATE UNIQUE INDEX "_SportsmanToSportsmanGroup_AB_unique" ON "_SportsmanToSportsmanGroup"("A" int4_ops,"B" int4_ops);
CREATE INDEX "_SportsmanToSportsmanGroup_B_index" ON "_SportsmanToSportsmanGroup"("B" int4_ops);