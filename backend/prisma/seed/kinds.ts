import { PrismaClient } from '@prisma/client'

export default async function seed () {
    const prisma = new PrismaClient()

    const kinds = [
        { name: 'Б/п', isApparatus: false },
        { name: 'Б/п 1', isApparatus: false },
        { name: 'Б/п 2', isApparatus: false },
        { name: 'Обруч', isApparatus: true },
        { name: 'Скакалка', isApparatus: true },
        { name: 'Мяч', isApparatus: true },
        { name: 'Булавы', isApparatus: true },
        { name: 'Лента', isApparatus: true },
        { name: 'Вид', isApparatus: true },
        { name: 'Вид 1', isApparatus: true },
        { name: 'Вид 2', isApparatus: true },
        { name: 'Вид 3', isApparatus: true },
        { name: 'Вид 4', isApparatus: true },
        { name: '5 скакалок', isApparatus: true },
        { name: '5 мячей', isApparatus: true },
        { name: '5 обручей', isApparatus: true },
        { name: '5 пар булав', isApparatus: true },
        { name: '3 обруча 2 пары булав', isApparatus: true },
        { name: '5 лент', isApparatus: true },
    ]
    await prisma.$transaction(kinds.map((kind, i) =>
        prisma.kind.upsert({
            where: { name: kind.name },
            update: {
                ...kind,
                createdAt: new Date(i)
            },
            create: {
                ...kind,
                createdAt: new Date(i)
            },
        })))

    await prisma.$disconnect()
}
