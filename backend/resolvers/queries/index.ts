import PrismaQueries from './prisma'
import JudgeConnection from './judgeConnections'

export default [PrismaQueries, JudgeConnection]