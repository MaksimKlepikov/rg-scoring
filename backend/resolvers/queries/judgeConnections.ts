import { extendType, idArg, arg, intArg, floatArg, stringArg } from '@nexus/schema'
import { EnumJudgeType, EnumJudgePanel, Score } from '@prisma/client'
import { Context } from '../../context'

export default extendType({
    type: "Query",
    definition (t) {
        t.field('judgeConnections', {
            type: 'JudgeConnection',
            list:true,
            resolve (root, args, ctx) {
                return ctx.judges
            }
        })
    }
})