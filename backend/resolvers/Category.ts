import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Category',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.name()
        t.model.type()
        t.model.events()
        t.model.sportsmen({
            ordering: true,
        })
        t.model.sportsmanGroups({
            ordering: true,
        })
        t.model.exportLabel()
        t.model.kinds({
            ordering: true,
        })
        t.model.kindsBestResults({
            ordering: true,
        })
    },
})