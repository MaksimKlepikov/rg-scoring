import { extendType, intArg, booleanArg, arg } from '@nexus/schema'
import { Rotation, Performance, RotationCreateInput, RotationWhereUniqueInput, PerformanceWhereInput, RotationUpdateInput } from '@prisma/client'
import { Context } from '../../context'

export default extendType({
    type: 'Mutation',
    definition (t) {
        t.field('deleteParticipantsInCategory', {
            type: 'Category',
            args: {
                categoryId: intArg({ required: true }),
                participantsIds: intArg({ required: true, list: true }),
                isSportsmanGroup: booleanArg({ required: true }),
            },
            nullable: true,
            resolve: async (_, { categoryId, participantsIds, isSportsmanGroup }: { categoryId: number, participantsIds: [number], isSportsmanGroup: Boolean }, ctx) => {

                const updatedCategory = await ctx.prisma.category.update({
                    where: {
                        id: categoryId
                    },
                    data: {
                        [isSportsmanGroup ? 'sportsmanGroups' : 'sportsmen']: {
                            deleteMany: {
                                id: {
                                    in: participantsIds
                                }
                            }
                        }
                    },
                    include: {
                        sportsmen: {
                            orderBy: {
                                numberInCategory: 'asc'
                            }
                        },
                        sportsmanGroups: {
                            orderBy: {
                                numberInCategory: 'asc'
                            }
                        }
                    }

                })
                const participants = isSportsmanGroup ? updatedCategory.sportsmanGroups : updatedCategory.sportsmen
                const participantsToUpdate = participants.map((p, i) => ({
                    where: {
                        id: p.id
                    },
                    data: {
                        numberInCategory: i + 1
                    }
                }))
                return ctx.prisma.category.update({
                    where: {
                        id: categoryId
                    },
                    data: {
                        [isSportsmanGroup ? 'sportsmanGroups' : 'sportsmen']: {
                            update: participantsToUpdate
                        }
                    },
                    include: {
                        sportsmen: {
                            orderBy: {
                                numberInCategory: 'asc'
                            }
                        },
                        sportsmanGroups: {
                            orderBy: {
                                numberInCategory: 'asc'
                            }
                        }
                    }

                })
            }
        });
        t.field('tossFromWorstToBest', {
            type: 'Category',
            args: {
                categoryId: intArg({ required: true }),
            },
            nullable: true,
            resolve: async (_, { categoryId }: { categoryId: number }, ctx) => {
                const category = await ctx.prisma.category.findOne({
                    where: {
                        id: categoryId
                    },
                    include: {
                        kindsBestResults: true,
                        sportsmen: {
                            include: {
                                performances: {
                                    include: {
                                        score: true,
                                        kind: true
                                    }
                                }
                            }
                        },
                        sportsmanGroups: {
                            include: {
                                performances: {
                                    include: {
                                        score: true,
                                        kind: true
                                    }
                                }
                            }
                        }
                    }
                })
                const isHaveKindBestResults = category.kindsBestResults?.length > 0
                const isSportsmanGroup = category.type === 'GROUP'
                const participants = isSportsmanGroup ? category.sportsmanGroups : category.sportsmen
                const sortedParticipants = participants.map(participant => ({
                    ...participant,
                    mainScore: (
                        isHaveKindBestResults
                            ? [
                                ...participant.performances
                                    .filter(performance => !category.kindsBestResults.some(kind => kind.id === performance.kind.id)),
                                participant.performances
                                    .filter(performance => category.kindsBestResults.some(kind => kind.id === performance.kind.id))
                                    .reduce((bestPerformance, performance) =>
                                        (!bestPerformance || bestPerformance.score.finalScore < performance.score.finalScore) ? performance : bestPerformance
                                        , null)
                            ]
                            :
                            participant.performances
                    ).reduce((mainScore, performance) => mainScore + (performance?.score?.finalScore ?? 0), 0)
                })).sort((p1, p2) => p2.mainScore - p1.mainScore).reverse()

                const participantsToUpdate = sortedParticipants.map((p, i) => ({
                    where: {
                        id: p.id
                    },
                    data: {
                        numberInCategory: i + 1
                    }
                }))
                return ctx.prisma.category.update({
                    where: {
                        id: categoryId
                    },
                    data: {
                        [isSportsmanGroup ? 'sportsmanGroups' : 'sportsmen']: {
                            update: participantsToUpdate
                        }
                    },
                    include: {
                        sportsmen: {
                            orderBy: {
                                numberInCategory: 'asc'
                            }
                        },
                        sportsmanGroups: {
                            orderBy: {
                                numberInCategory: 'asc'
                            }
                        }
                    }

                })
            }
        });
    }
})
