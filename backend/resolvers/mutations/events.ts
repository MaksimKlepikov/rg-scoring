import { extendType, intArg, arg } from '@nexus/schema'
import { EventWhereUniqueInput, EventUpdateInput } from '@prisma/client'

export default extendType({
    type: 'Mutation',
    definition (t) {
        t.field('deleteEvent', {
            type: 'Event',
            nullable: false,
            args: { eventId: intArg({ required: true }) },
            resolve: async (_, { eventId }: { eventId: number }, ctx) => {
                const results = await ctx.prisma.$transaction([
                    ctx.prisma.judgeSeat.deleteMany({
                        where: {
                            event: {
                                id: eventId
                            }
                        }
                    }),
                    ctx.prisma.event.delete({
                        where: {
                            id: eventId
                        }
                    })
                ])
                return results[1]

            }
        });
        t.field('updateEvent', {
            type: 'Event',
            nullable: false,
            args: { where: arg({ type: 'EventWhereUniqueInput', required: true }), data: arg({ type: 'EventUpdateInput', required: true }) },
            resolve: async (_, { where, data }: { where: EventWhereUniqueInput, data: EventUpdateInput }, ctx) => {
                const updatedEvent = await ctx.prisma.event.update({where,data})

                ctx.pubsub.publish("EVENT_UPDATED", {
                    eventUpdated: updatedEvent
                })
                return updatedEvent

            }
        });
    },
})