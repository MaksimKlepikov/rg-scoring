import { extendType, intArg, booleanArg, arg } from '@nexus/schema'
import { Rotation, Performance, RotationCreateInput, RotationWhereUniqueInput, PerformanceWhereInput, RotationUpdateInput, EnumJudgePanel, PerformanceWhereUniqueInput, PerformanceUpdateInput } from '@prisma/client'
import { Context } from '../../context'

export default extendType({
    type: 'Mutation',
    definition (t) {
        t.field('addPerformancesToRotation', {
            type: 'Rotation',
            list: true,
            args: {
                where: arg({ type: 'RotationWhereUniqueInput', required: true }),
                data: arg({ type: 'RotationUpdateInput', required: true })
            },
            nullable: true,
            resolve: async (_, { where, data }: { where: RotationWhereUniqueInput, data: RotationUpdateInput }, ctx) => {
                const updatedRotation = await ctx.prisma.rotation.update({
                    where,
                    data
                })
                const dayId = updatedRotation.dayId
                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)
            }
        });
        t.field('changePerformanceJudgePanel', {
            type: 'Performance',
            args: {
                performanceId: intArg({ required: true }),
                judgePanel: arg({ type: 'EnumJudgePanel', required: true })
            },
            nullable: true,
            resolve: async (_, { performanceId, judgePanel }: { performanceId: number, judgePanel: EnumJudgePanel }, ctx) => {
                const updatedPerformance = await ctx.prisma.performance.update({
                    where: { id: performanceId },
                    data: { judgePanel },
                })
                ctx.pubsub.publish("MANY_PERFORMANCES_UPDATED", {
                    day: { id: updatedPerformance.dayId }
                })
                return updatedPerformance
            }
        });
        t.field('updatePerformance', {
            type: 'Performance',
            args: {
                where: arg({ type: 'PerformanceWhereUniqueInput', required: true }),
                data: arg({ type: 'PerformanceUpdateInput', required: true })
            },
            nullable: true,
            resolve: async (_, { where, data }: { where: PerformanceWhereUniqueInput, data: PerformanceUpdateInput }, ctx) => {
                const updatedPerformance = await ctx.prisma.performance.update({
                    where,
                    data,
                })
                ctx.pubsub.publish("PERFORMANCE_UPDATED", {
                    performanceUpdated: updatedPerformance
                })
                return updatedPerformance
            }
        });
        t.field('deleteRotation', {
            type: 'Rotation',
            list: true,
            args: { where: arg({ type: 'RotationWhereUniqueInput', required: true }) },
            nullable: true,
            resolve: async (_, { where }: { where: RotationWhereUniqueInput }, ctx) => {
                await ctx.prisma.performance.deleteMany({
                    where: {
                        subRotation: {
                            rotation: {
                                id: where.id
                            }
                        }
                    }
                })
                await ctx.prisma.subRotation.deleteMany({
                    where: {
                        rotation: {
                            id: where.id
                        }
                    }
                })
                const deletedRotation = await ctx.prisma.rotation.delete({ where })
                const dayId = deletedRotation.dayId
                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)
            }
        });
        t.field('deletePerformances', {
            type: 'Rotation',
            list: true,
            args: { where: arg({ type: 'PerformanceWhereInput', required: true }) },
            nullable: true,
            resolve: async (_, { where }: { where: PerformanceWhereInput }, ctx) => {
                const rotations = await ctx.prisma.rotation.findMany({
                    where: {
                        subRotations: {
                            some: {
                                performances: {
                                    some: {
                                        id: where?.id
                                    }
                                }
                            }
                        }
                    }
                })
                const dayId = rotations?.[0].dayId
                await ctx.prisma.performance.deleteMany({ where })
                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)
            }
        });
        t.field('reorderRotation', {
            type: 'Rotation',
            list: true,
            args: { rotationId: intArg(), newNumber: intArg(), isSwap: booleanArg() },
            nullable: true,
            resolve: async (_, { rotationId, newNumber, isSwap }: { rotationId: number, newNumber: number, isSwap: boolean }, ctx) => {
                if (newNumber < 1) {
                    newNumber = 1
                }
                const selectedRotation = await ctx.prisma.rotation.findOne({
                    where: {
                        id: rotationId
                    },
                    include: {
                        day: true
                    }
                })
                const dayId = selectedRotation?.day?.id
                if (!dayId)
                    return []
                const rotations = await ctx.prisma.rotation.findMany({
                    where: {
                        day: {
                            id: dayId
                        }
                    },
                    orderBy: {
                        mainNumber: 'asc'
                    }
                })
                let toRotationNumberUpdate = []

                let newRotations: Rotation[] = []
                const selectedRotationIndex = rotations.findIndex(rotation => rotation.id === rotationId)
                const toRotationIndex = rotations.findIndex(rotation => rotation.mainNumber >= newNumber)

                if (isSwap) {
                    const temp = rotations[toRotationIndex];
                    rotations[toRotationIndex] = rotations[selectedRotationIndex];
                    rotations[selectedRotationIndex] = temp;
                    toRotationNumberUpdate = [rotations[selectedRotationIndex], rotations[toRotationIndex]]
                    newRotations = rotations
                } else {
                    newRotations = rotations.reduce<Rotation[]>((newRotations, rotation, i) => {
                        if (i === selectedRotationIndex)
                            return newRotations

                        if (i === toRotationIndex)
                            if (toRotationIndex <= selectedRotationIndex)
                                return [...newRotations, rotations[selectedRotationIndex], rotation]
                            else
                                return [...newRotations, rotation, rotations[selectedRotationIndex]]

                        return [...newRotations, rotation]
                    }, [])
                }
                toRotationNumberUpdate = newRotations.filter((rotation, i) => {
                    if (rotation.mainNumber !== i + 1) {
                        rotation.mainNumber = i + 1
                        return true
                    }
                    return false
                })
                await ctx.prisma.$transaction(
                    toRotationNumberUpdate.map(({ id, mainNumber }) =>
                        ctx.prisma.rotation.update({
                            where: {
                                id
                            },
                            data: {
                                mainNumber
                            }
                        }))
                )

                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)

            }
        });
        t.field('reorderPerformances', {
            type: 'Rotation',
            list: true,
            args: { where: arg({ type: 'ReorderPerformanceInputType', list: true }) },
            nullable: true,
            resolve: async (_, { where }: { where: { performanceId: number, subRotationToId: number, nextPerformanceId: number }[] }, ctx) => {
                const subRotationToId = where[0].subRotationToId
                const subRotation = await ctx.prisma.subRotation.findOne({
                    where: { id: subRotationToId }, include: {
                        performances: {
                            orderBy: {
                                numberInRotation: 'asc'
                            }
                        },
                        rotation: true
                    }
                })
                if (!subRotation) {
                    return
                }
                const dayId = subRotation?.rotation.dayId

                await Promise.all(
                    where.map(({ performanceId, subRotationToId, nextPerformanceId }) => reorderPerformance(ctx, performanceId, subRotationToId, nextPerformanceId))
                )
                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)
            }
        });
        t.field('swapPerformances', {
            type: 'Rotation',
            list: true,
            args: { where: arg({ type: 'SwapPerformanceInputType', list: true }) },
            nullable: true,
            resolve: async (_, { where }: { where: { performance1Id: number, performance2Id: number }[] }, ctx) => {
                const day = await ctx.prisma.day.findFirst({
                    where: {
                        rotations: {
                            some: {
                                subRotations: {
                                    some: {
                                        performances: {
                                            some: {
                                                id: where[0].performance1Id
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
                if (!day) {
                    return
                }
                const dayId = day.id

                await Promise.all(
                    where.map(({ performance1Id, performance2Id }) => swapPerformance(ctx, performance1Id, performance2Id))
                )
                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)
            }
        });
        t.field('createRotations', {
            type: 'Rotation',
            args: {
                data: arg({ type: 'RotationCreateInput', required: true, list: true })
            },
            nullable: true,
            list: true,
            resolve: async (_, { data }: { data: [RotationCreateInput] }, ctx) => {
                const dayId = data[0]?.day?.connect?.id
                if (!dayId)
                    return []
                const rotations = await ctx.prisma.rotation.findMany({
                    where: {
                        day: {
                            id: dayId
                        }
                    },
                    orderBy: {
                        mainNumber: 'asc'
                    }
                })
                const createdRotations = await ctx.prisma.$transaction(
                    data.map(rotation => ctx.prisma.rotation.create({ data: rotation }))
                )

                let isNewRotationsInserted = false
                const newRotations = rotations.reduce<Rotation[]>((newRotations, currentRotation, i) => {
                    if (isNewRotationsInserted) {
                        return [...newRotations, currentRotation]
                    }
                    if (createdRotations[0].mainNumber <= currentRotation.mainNumber) {
                        isNewRotationsInserted = true
                        return [...newRotations, ...createdRotations, currentRotation]
                    }
                    if (rotations.length === i + 1) {
                        isNewRotationsInserted = true
                        return [...newRotations, currentRotation, ...createdRotations]
                    }
                    return [...newRotations, currentRotation]
                }, [])


                const toRotationNumberUpdate = newRotations.filter((rotation, i) => {
                    if (rotation.mainNumber !== i + 1) {
                        rotation.mainNumber = i + 1
                        return true
                    }
                    return false
                })

                await ctx.prisma.$transaction(
                    toRotationNumberUpdate.map(({ id, mainNumber }) =>
                        ctx.prisma.rotation.update({
                            where: {
                                id
                            },
                            data: {
                                mainNumber
                            }
                        }))
                )

                await fixRotationsPerformancesOrder(dayId, ctx)
                return getDayRotations(ctx, dayId)

            }
        });
    },
})

async function fixRotationsPerformancesOrder (dayId: number, ctx: Context) {
    const dayRotations = await ctx.prisma.rotation.findMany({
        where: {
            day: { id: dayId }
        },
        orderBy: {
            mainNumber: 'asc'
        },
        include: {
            subRotations: {
                include: {
                    performances: {
                        orderBy: {
                            numberInRotation: 'asc'
                        }
                    }
                }
            }
        }
    })

    const toRotationNumberUpdate = dayRotations.filter((rotation, i) => {
        if (rotation.mainNumber !== i + 1) {
            rotation.mainNumber = i + 1
            return true
        }
        return false
    })
    let currentPerformancesNumber = 0
    let currentPerformancesNumberInRotation = 0
    const toPerformanceNumberUpdate = dayRotations.reduce<Performance[]>((toPerformanceNumberUpdate, rotation) => {
        currentPerformancesNumberInRotation = 0
        return rotation.subRotations.reduce((toPerformanceNumberUpdate, subRotation) => [
            ...toPerformanceNumberUpdate,
            ...subRotation.performances.filter((performance, i) => {
                currentPerformancesNumber++
                currentPerformancesNumberInRotation++
                if (performance.mainNumber !== currentPerformancesNumber || performance.numberInRotation !== currentPerformancesNumberInRotation) {
                    performance.mainNumber = currentPerformancesNumber
                    performance.numberInRotation = currentPerformancesNumberInRotation
                    return true
                }
                return false
            })], toPerformanceNumberUpdate)
    }, [])
    await ctx.prisma.$transaction(
        toRotationNumberUpdate.map(({ id, mainNumber }) =>
            ctx.prisma.rotation.update({
                where: {
                    id
                },
                data: {
                    mainNumber
                }
            }))
    )
    await ctx.prisma.$transaction(
        toPerformanceNumberUpdate.map(({ id, mainNumber, numberInRotation }) =>
            ctx.prisma.performance.update({
                where: {
                    id
                },
                data: {
                    mainNumber,
                    numberInRotation
                }
            }))
    )

    ctx.pubsub.publish("MANY_PERFORMANCES_UPDATED", {
        day: { id: dayId }
    })

}

const getDayRotations = (ctx: Context, dayId: number) => {

    return ctx.prisma.rotation.findMany({
        where: {
            day: { id: dayId }
        },
        orderBy: {
            mainNumber: 'asc'
        },
        include: {
            subRotations: {
                include: {
                    performances: {
                        orderBy: {
                            mainNumber: 'asc'
                        }
                    }
                }
            }
        }
    })
}

const reorderPerformance = async (ctx: Context, performanceId: number, subRotationToId: number, nextPerformanceId: number) => {

    const subRotation = await ctx.prisma.subRotation.findOne({
        where: { id: subRotationToId }, include: {
            performances: {
                orderBy: {
                    numberInRotation: 'asc'
                }
            },
            rotation: true
        }
    })
    if (!subRotation) {
        return
    }
    await ctx.prisma.performance.update({
        where: { id: performanceId }, data: {
            subRotation: {
                connect: {
                    id: subRotationToId
                }
            },
            numberInRotation: 0
        }
    })
    const subRotationPerformances = subRotation.performances.filter(({ id }) => id !== performanceId)
    if (subRotationPerformances.length > 0) {
        if (nextPerformanceId === null || nextPerformanceId === undefined) {
            await ctx.prisma.performance.update({
                where: { id: performanceId }, data: {
                    numberInRotation: subRotationPerformances[subRotationPerformances.length - 1].numberInRotation + 1
                }
            })
        } else {
            const nextPerformanceIndex = subRotationPerformances.findIndex(performance => performance.id === nextPerformanceId) ?? -1
            if (nextPerformanceIndex > -1) {
                const performancesToUpdate = [...subRotationPerformances.slice(0, nextPerformanceIndex), { id: performanceId }, ...subRotationPerformances.slice(nextPerformanceIndex)]
                    .reduce((performancesToUpdate, performance, i) => {
                        if (performance.numberInRotation !== subRotationPerformances[0].numberInRotation + i) {
                            performancesToUpdate.push({
                                ...performance,
                                numberInRotation: subRotationPerformances[0].numberInRotation + i
                            })
                        }
                        return performancesToUpdate
                    }, [])
                await ctx.prisma.subRotation.update({
                    where: { id: subRotationToId }, data: {
                        performances: {
                            update: performancesToUpdate.map(({ id, numberInRotation }) => ({
                                where: { id },
                                data: { numberInRotation }
                            }))
                        }
                    }
                })
            }
        }
    }
}
const swapPerformance = async (ctx: Context, performance1Id: number, performance2Id: number) => {

    const performance1 = await ctx.prisma.performance.findOne({
        where: { id: performance1Id }, include: {
            subRotation: true,
        }
    })
    const performance2 = await ctx.prisma.performance.findOne({
        where: { id: performance2Id }, include: {
            subRotation: true,
        }
    })
    if (!performance1 || !performance2) {
        return
    }

    await ctx.prisma.$transaction([
        ctx.prisma.performance.update({
            where: {
                id: performance1.id
            },
            data: {
                numberInRotation: performance2.numberInRotation,
                subRotation: {
                    connect: {
                        id: performance2.subRotation.id
                    }
                }
            }
        }),
        ctx.prisma.performance.update({
            where: {
                id: performance2.id
            },
            data: {
                numberInRotation: performance1.numberInRotation,
                subRotation: {
                    connect: {
                        id: performance1.subRotation.id
                    }
                }
            }
        })
    ])
}