import { intArg, arg, extendType, } from '@nexus/schema'
import { withFilter } from 'graphql-subscriptions'
import { Context, setJudges } from '../context'
export default extendType({
    type: 'Subscription',

    definition (t) {

        t.field("judgePanelUpdated", {
            type: "JudgeSeat",
            args: {
                dayId: intArg({ required: true }),
                judgePanel: arg({
                    type: 'EnumJudgePanel',
                    required: true,
                }),
            },
            list: true,
            subscribe: withFilter(
                (_parent, _args, ctx) => {
                    return ctx.pubsub.asyncIterator("JUDGE_PANEL_UPDATED")
                },
                (payload, { dayId, judgePanel }) => {
                    return payload.judgePanelUpdated.some(judgeSeat => judgeSeat.event.days.some(({ id }) => id == dayId) && judgeSeat.panel === judgePanel)
                }
            ),
        })
        t.field("judgeSeatsUpdated", {
            type: "JudgeSeat",
            args: {
                judgeSeatId: intArg({ required: true }),
            },
            list: true,
            subscribe: withFilter(
                (_parent, _args, ctx: Context) => {
                    setJudges([{
                        uuid: ctx.uuid,
                        judgeSeatId: _args.judgeSeatId,
                    }, ...ctx.judges.filter(judge => judge.uuid !== ctx.uuid)])
                    return ctx.pubsub.asyncIterator("JUDGE_SEATS_UPDATED")
                },
                (payload, { judgeSeatId }) => payload.judgeSeatsUpdated.some(judgeSeatUpdated => judgeSeatUpdated.id === judgeSeatId)
            ),
        })
        t.field("scoreUpdated", {
            type: "Score",
            args: {
                dayId: intArg({ required: true }),
                judgePanel: arg({
                    type: 'EnumJudgePanel',
                    required: true,
                }),
            },
            subscribe: withFilter(
                (_parent, _args, ctx) => {
                    return ctx.pubsub.asyncIterator("SCORE_UPDATED")
                },
                (payload, { dayId, judgePanel }) => {
                    return payload.scoreUpdated.performance.day.id == dayId
                        && payload.scoreUpdated.performance.judgePanel == judgePanel
                }
            ),
        })
        t.field("eventUpdated", {
            type: "Event",
            args: {
                eventId: intArg({ required: true }),
            },
            subscribe: withFilter(
                (_parent, _args, ctx) => {
                    return ctx.pubsub.asyncIterator("EVENT_UPDATED")
                },
                (payload, { eventId }) => payload.eventUpdated.id == eventId
            ),
        })
        t.field("performanceShowed", {
            type: "Performance",
            args: {
                eventId: intArg({ required: true }),
            },
            subscribe: withFilter(
                (_parent, _args, ctx) => {
                    return ctx.pubsub.asyncIterator("PERFORMANCE_SHOWED")
                },
                (payload, { eventId }) => payload.performanceShowed.day.event.id == eventId
            ),
        })
        t.field("manyPerformancesUpdated", {
            type: "Day",
            args: {
                dayId: intArg({ required: true }),
            },
            nullable: true,
            subscribe: withFilter(
                (_parent, _args, ctx) => {
                    return ctx.pubsub.asyncIterator("MANY_PERFORMANCES_UPDATED")
                },
                (payload, { dayId }) => payload.day.id == dayId
            ),
        })
        t.field("performanceUpdated", {
            type: "Performance",
            args: {
                dayId: intArg({ required: true }),
            },
            nullable: true,
            subscribe: withFilter(
                (_parent, _args, ctx) => {
                    return ctx.pubsub.asyncIterator("PERFORMANCE_UPDATED")
                },
                (payload, { dayId }) => payload.performanceUpdated.dayId == dayId
            ),
        })
    }
})