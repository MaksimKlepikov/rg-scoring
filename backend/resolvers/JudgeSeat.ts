import { objectType } from '@nexus/schema'

export default objectType({
    name: 'JudgeSeat',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.currentPerformance()
        t.model.type()
        t.model.panel()
        t.model.event()
    },
})