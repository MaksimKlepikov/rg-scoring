import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Rotation',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.mainNumber()
        t.model.name()
        t.model.day()
        t.model.subRotations({
            ordering: true,
        })
        t.model.categoriesOfFirstJudgePanel()
        t.model.categoriesOfSecondJudgePanel()
        t.model.kindsOfFirstJudgePanel()
        t.model.kindsOfSecondJudgePanel()
        t.model.mix()
    },
})