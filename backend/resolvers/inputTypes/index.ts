
import { inputObjectType } from '@nexus/schema'
export const ReorderPerformanceInputType = inputObjectType({
    name: 'ReorderPerformanceInputType',
    definition (t) {
        t.int('performanceId', { required: true })
        t.int('subRotationToId')
        t.int('nextPerformanceId')
    },
})
export const SwapPerformanceInputType = inputObjectType({
    name: 'SwapPerformanceInputType',
    definition (t) {
        t.int('performance1Id', { required: true })
        t.int('performance2Id', { required: true })
    },
})