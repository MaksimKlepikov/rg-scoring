import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Kind',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.name()
        t.model.isApparatus()
        t.model.performances()
        t.model.categories()
    },
})