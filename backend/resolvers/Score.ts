import { objectType } from '@nexus/schema'

export default objectType({
    name: 'Score',
    definition (t) {
        t.model.id()
        t.model.createdAt()
        t.model.updatedAt()
        t.model.performance()
        t.model.d1()
        t.model.d2()
        t.model.d3()
        t.model.d4()
        t.model.e1_2()
        t.model.e3()
        t.model.e4()
        t.model.e5()
        t.model.e6()
        t.model.eAverage()
        t.model.finalD()
        t.model.finalE()
        t.model.finalPenalty()
        t.model.finalScore()
    },
})