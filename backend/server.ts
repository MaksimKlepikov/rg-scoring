
import { ApolloServer } from 'apollo-server'
import schema from './schema'
import { createContext, judges, setJudges } from './context'
import { v4 as uuid } from 'uuid';
import { config } from 'dotenv'
config({ path: './prisma/.envDocker' })



new ApolloServer({
    schema,
    context: createContext,
    subscriptions: {
        onConnect: (connectionParams, webSocket, context) => {
            return {
                uuid: uuid()
            }
        },
        onDisconnect: async (webSocket, context) => {
            const initialContext = await context.initPromise;
            setJudges(judges.filter(judge => judge.uuid !== initialContext.uuid))
        },
    },
}).listen(
    { port: 4000 },
).then(({ url, subscriptionsUrl }) => {
    console.log(`🚀 Server ready at ${url}`);
    console.log(`🚀 Subscriptions ready at ${subscriptionsUrl}`);
});