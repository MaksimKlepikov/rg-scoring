import { PrismaClient } from '@prisma/client'
import { PubSub } from 'apollo-server'

type JudgeConnection = {
    uuid: string,
    judgeSeatId: number
}

const prisma = new PrismaClient()
const pubsub = new PubSub()
export let judges: JudgeConnection[] = []

export type Context = {
    prisma: PrismaClient,
    pubsub: PubSub,
    judges: JudgeConnection[],
    uuid:string
}

export const createContext = ({connection}): Context => {
    const subscriptionContext = connection?.context
    return {
        prisma,
        pubsub,
        judges,
        ...subscriptionContext
    }
}

export const setJudges = (newJudges: JudgeConnection[])=>{
    judges = newJudges
}