import * as NexusSchema from '@nexus/schema'
import { nexusSchemaPrisma } from 'nexus-plugin-prisma/schema'
import * as path from 'path'
import * as Mutations from './resolvers/mutations'
import * as Queries from './resolvers/queries'
import * as Event from './resolvers/Event'
import * as Category from './resolvers/Category'
import * as City from './resolvers/City'
import * as Country from './resolvers/Country'
import * as Day from './resolvers/Day'
import * as JudgeSeat from './resolvers/JudgeSeat'
import * as Kind from './resolvers/Kind'
import * as Performance from './resolvers/Performance'
import * as Region from './resolvers/Region'
import * as Rotation from './resolvers/Rotation'
import * as SubRotation from './resolvers/SubRotation'
import * as Score from './resolvers/Score'
import * as Sportsman from './resolvers/Sportsman'
import * as SportsmanGroup from './resolvers/SportsmanGroup'
import * as ExportConfig from './resolvers/ExportConfig'
import * as Subscription from './resolvers/Subscription'
import * as Inputs from './resolvers/inputTypes'
import * as JudgeConnection from './resolvers/JudgeConnection'

export default NexusSchema.makeSchema({
    types: [
        Inputs,
        Queries, Mutations,
        Subscription,
        Event,
        Category,
        City,
        Country,
        Day,
        JudgeSeat,
        Kind,
        Performance,
        Region,
        Rotation,
        SubRotation,
        Score,
        Sportsman,
        SportsmanGroup,
        ExportConfig,
        JudgeConnection
    ],
    plugins: [
        nexusSchemaPrisma({
            experimentalCRUD: true,
        }),
        NexusSchema.connectionPlugin()
    ],
    outputs: {
        typegen: path.join(
            __dirname,
            './node_modules/@types/nexus-typegen/index.d.ts',
        ),
    },
    typegenAutoConfig: {
        contextType: 'Context.Context',
        sources: [
            {
                source: '.prisma/client',
                alias: 'prisma',
            },
            {
                source: require.resolve('./context'),
                alias: 'Context',
            },
        ],
    },
})