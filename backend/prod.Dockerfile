FROM node:12-alpine
RUN apk add --no-cache --virtual .build-deps \
    python \
    make \
    g++
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn
RUN apk del .build-deps
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY . ./
RUN yarn build