const formatForUpdateMixin = {
  methods: {
    formatForUpdate (value) {
      const formatedValue = {}
      for (const key in value) {
        if (Object.prototype.hasOwnProperty.call(value, key)) {
          formatedValue[key] = { set: value[key] }
        }
      }
      return formatedValue
    }
  }
}
export default formatForUpdateMixin
