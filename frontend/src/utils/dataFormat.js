const formatScore = (score) => {
  // return Math.floor(score * 1000) / 1000
  if (score) {
    return +score.toFixed(2)
  }
  return score
}
const truncateString = (value, maxLength) => {
  return (value.length > maxLength) ? value.substr(0, maxLength - 3) + '...' : value
}

const formatSportsmanToCreate = (sportsman, numberInCategory = 0) => {
  return {
    ...sportsman,
    id: undefined,
    country: sportsman.country
      ? {
          connect: {
            name: sportsman.country.name ?? sportsman.country
          }
        }
      : undefined,
    region: sportsman.region
      ? {
          connect: {
            name: sportsman.region.name ?? sportsman.region
          }
        }
      : undefined,
    city: sportsman.city
      ? {
          connect: {
            name: sportsman.city.name ?? sportsman.city
          }
        }
      : undefined,
    numberInCategory,
    rowNumber: undefined,
    __index: undefined
  }
}

const formatSportsmanGroupToCreate = (sportsmanGroup, numberInCategory = 0) => {
  return {
    ...sportsmanGroup,
    id: undefined,
    country: sportsmanGroup.country
      ? {
          connect: {
            name: sportsmanGroup.country.name ?? sportsmanGroup.country
          }
        }
      : undefined,
    region: sportsmanGroup.region
      ? {
          connect: {
            name: sportsmanGroup.region.name ?? sportsmanGroup.region
          }
        }
      : undefined,
    city: sportsmanGroup.city
      ? {
          connect: {
            name: sportsmanGroup.city.name ?? sportsmanGroup.city
          }
        }
      : undefined,
    sportsmen: {
      create: sportsmanGroup.sportsmen.map((item, i) => formatSportsmanToCreate(item, i + 1))
    },
    numberInCategory,
    rowNumber: undefined,
    __index: undefined
  }
}

export {
  formatScore,
  truncateString,
  formatSportsmanToCreate,
  formatSportsmanGroupToCreate
}
