// import Vue from 'vue'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'

// "async" is optional
// Vue.use(Notifications)
export default async ({ Vue }) => {
  Vue.use(Notifications, { velocity })
}
