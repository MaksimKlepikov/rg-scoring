import { createHttpLink } from 'apollo-link-http'

import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

const httpLink = createHttpLink({
  // You should use an absolute URL here
  uri: '/api'
})

const wsLink = new WebSocketLink({
  uri: 'ws://' + location.host + '/graphql',
  options: {
    reconnect: true
  }
})

const link = split(
  // split based on operation type
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
  },
  wsLink,
  httpLink
)

export { link }
