// import gql from 'graphql-tag'
import allEventsQuery from 'src/graphql/queries/allEvents.gql'
import updateEvent from 'src/graphql/mutations/updateEvent.gql'

export const ALL_EVENTS_QUERY = allEventsQuery
export const UPDATE_EVENT_MUTATION = updateEvent
